# Emoji Encryption
[![IMAGE ALT TEXT HERE](media/pycharm64_Fh7lEv2jFe.png)](https://youtu.be/YzAkIPjhzzQ)
## PL
Algorytm szyfruje wiadomość pod postacią emoji. Celem tego algorytmu jest szyfrowanie wiadomości pod postacią emoji, lecz można także zaszyfrować emoji pod postacią znaków. 
### Użycie
- Żeby zaszyfrować wiadomosć należy dodać do wiadomości ?szyfruj?
- Żeby zdeszyfrować wiadomość należy dodać do wiadomości ?deszyfruj?
- Między wiadomością a kluczem nie powinno być przerwy
### Działanie
- Algorytm dzieli wiadomość na 10-znakowe segmenty + reszta
- Algorytm miesza znaki wewnątrz każdego segmentu
- Algorytm miesza segmenty
- Algorytm podstawia emoji pod znaki
### Instalacja
```
conda create --name <env> --file requirements.txt
```
Plik .exe znajduje się w [folderze dist](dist)

## EN
The algorithm encrypts the message in the form of emoji. The purpose of this algorithm is to encrypt messages in the form of emoji, but you can also encrypt emoji in the form of characters.
### Use
- To encrypt a message you need to add ?szyfruj?
- To encrypt a message you need to add to the message ?deszyfruj?
- There should be no gap between the message and the key
### Action
- The algorithm divides the message into 10-character segments + the rest
- The algorithm mixes characters inside each segment
- The algorithm mixes the segments
- The algorithm puts emoji under the signs
### Installation
```
conda create --name <env> --file requirements.txt
```
The .exe file is located in the [dist](dist)
