import itertools

from cryptor import Cryptor


class Encryptor(Cryptor):

    def encrypt(self, data_to_encrypt: str) -> str:
        """
        1. Remove ?szyfruj? from string
        2. Divide segments (10 chars segments)
        3. Shuffle with seed chars inside each segment
        4. Shuffle with seed segments
        5. Set emojis instead chars
        :param data_to_encrypt: string with ?szyfruj?
        :return: encrypted string
        """

        data = self._remove_unwanted_data(data_to_encrypt)
        divided_data = self._divide_data(data)

        shuffled_str_in_segments = [self._shuffle_str_under_seed(segment) for segment in divided_data]
        shuffled_segments = self._shuffle_list_under_seed(shuffled_str_in_segments)
        merged_shuffle = "".join(itertools.chain(shuffled_segments))

        emoji_string = self._swap_emojis_chars(merged_shuffle)
        return emoji_string





