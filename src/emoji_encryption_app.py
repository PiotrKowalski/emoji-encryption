from typing import NoReturn
from console import Console
from encryptor import Encryptor
from decryptor import Decryptor
from enum import Enum
import re


class OperationEnum(Enum):
    """
    What operation should be done to the string
    """
    DO_NOTHING = 0
    SZYFRUJ = 1
    DESZYFRUJ = 2


class EmojiEncryptionApp:

    def __init__(self):
        self.console = Console()
        self.encryptor = Encryptor()
        self.decryptor = Decryptor()

    def run(self) -> NoReturn:
        """
        1. Check input
        2. Make operation on input (encrypt or decrypt)
        3. Output the result
        """
        while True:
            data = self.console.read_console()
            output_data = self._encrypt_or_decrypt(data)
            self.console.output_console(output_data)

    def _encrypt_or_decrypt(self, data: str) -> str:
        """
        Make operation on data
        :param data: data to have operation on
        :return:
        """
        operation_type = self._get_operation_type(data)

        if operation_type == OperationEnum.SZYFRUJ:
            return self.encryptor.encrypt(data)

        elif operation_type == OperationEnum.DESZYFRUJ:
            return self.decryptor.decrypt(data)

        else:
            return 'Wrong message. No ?szyfruj? or ?deszyfruj?'

    @staticmethod
    def _get_operation_type(data: str) -> OperationEnum:
        """
        Choose type of operation
        :param data: data to have operation on
        :return: OperationEnum
        """

        if re.search('\?szyfruj\?', data):
            return OperationEnum.SZYFRUJ

        elif re.search('\?deszyfruj\?', data):
            return OperationEnum.DESZYFRUJ
