from abc import ABC
import random
import string
from typing import Dict, List
import re


class TwoWayDict(dict):
    def __setitem__(self, key, value):
        # Remove any previous connections with these values
        if key in self:
            del self[key]
        if value in self:
            del self[value]
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)

    def __len__(self):
        """Returns the number of connections"""
        return dict.__len__(self) // 2


class Cryptor(ABC):
    def __init__(self):
        self.seed = 550
        self.encryption_dict = self.get_encryption_dict()

    def get_encryption_dict(self) -> Dict[str, str]:
        data = TwoWayDict()
        for char, emoji in zip(self._get_ascii(), self._get_emojis()):
            data[char] = emoji
        return data

    @staticmethod
    def _get_ascii():
        return string.ascii_letters + string.digits + string.punctuation + string.whitespace

    @staticmethod
    def _get_emojis() -> str:
        return '😀😃😄😁😆😅😂🤣🥲☺😊😇🙂🙃😉😌😍🥰😘😗😙😚😋😛😝😜🤪🤨🧐🤓😎🥸🤩🥳😏😒😞😔😟😕🙁☹' \
               '😣😖😫😩🥺😢😭😤😠😡🤬🤯😳🥵🥶😱😨😰😥😓🤗🤔🤭🤫🤥😶😐😑😬🙄😯😦😧😮😲🥱😴🤤😪😵🤐🥴🤢🤮' \
               '🤧😷🤒🤕🤑🤠😈👿👹👺🤡💩👻💀☠👽👾🤖🎃😺😸😹😻😼😽🙀😿😾'

    @staticmethod
    def _remove_unwanted_data(data_to_encrypt: str) -> str:
        return re.sub(r'\?szyfruj\?|\?deszyfruj\?', '', data_to_encrypt)

    @staticmethod
    def _divide_data(data: str) -> List[str]:
        return re.findall('.{1,10}', data)

    def _shuffle_str_under_seed(self, string: str):
        # Shuffle the string ls using the seed `seed`
        string = [i for i in string]
        random.seed(self.seed)
        random.shuffle(string)
        return ''.join(string)

    def _shuffle_list_under_seed(self, ls: List[str]) -> List[str]:
        # Shuffle the list ls using the seed `seed`
        random.seed(self.seed)
        random.shuffle(ls)
        return ls

    def _swap_emojis_chars(self, input_data):
        output_data = ''
        for i in input_data:
            output_data += self.encryption_dict[i]
        return output_data
