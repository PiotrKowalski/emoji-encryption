from typing import List
from cryptor import Cryptor
import itertools


class Decryptor(Cryptor):

    def decrypt(self, data_to_decrypt: str) -> str:
        """
        1. Remove ?deszyfruj? from string
        2. Set chars instead emojis
        3. Divide segments (10 chars segments)
        4. Shuffle with seed segments
        3. Shuffle with seed chars inside each segment
        :param data_to_decrypt: string with ?deszyfruj?
        :return: decrypted string
        """
        data_to_decrypt = self._remove_unwanted_data(data_to_decrypt)
        unset_emoji_data = self._swap_emojis_chars(data_to_decrypt)

        divided_data = self._divide_data(unset_emoji_data)
        unshuffled_segments = self._unshuffle_list_under_seed(divided_data)

        decrypted_data = [self._unshuffle_str_under_seed(segment) for segment in unshuffled_segments]
        merged_decrypted = "".join(itertools.chain(decrypted_data))

        return merged_decrypted

    def _unshuffle_str_under_seed(self, shuffled: str):
        n = len(shuffled)
        # Perm is [1, 2, ..., n]
        perm = [i for i in range(1, n + 1)]
        # Apply sigma to perm
        shuffled_perm = self._shuffle_list_under_seed(perm)
        # Zip and unshuffle
        zipped = list(zip(shuffled, shuffled_perm))
        zipped.sort(key=lambda x: x[1])
        return ''.join([a for (a, b) in zipped])

    def _unshuffle_list_under_seed(self, shuffled: List[str]):
        n = len(shuffled)
        # Perm is [1, 2, ..., n]
        perm = [i for i in range(1, n + 1)]
        # Apply sigma to perm
        shuffled_perm = self._shuffle_list_under_seed(perm)
        # Zip and unshuffle
        zipped = list(zip(shuffled, shuffled_perm))
        zipped.sort(key=lambda x: x[1])
        return [a for (a, b) in zipped]
