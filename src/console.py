import re


class Console:

    def read_console(self) -> str:
        """
        Read console input
        :return: string to have operation on
        """
        while True:
            data = input('Text for encryption or decryption (Add somewhere: ?szyfruj? or ?deszyfruj?): ')

            if self.__check_if_valid_input_console(data):
                return data

    @staticmethod
    def __check_if_valid_input_console(data_to_check: str) -> bool:
        reg = re.compile('\?szyfruj\?|\?deszyfruj\?')

        if reg.search(data_to_check):
            return True

        else:
            return False

    @staticmethod
    def output_console(data):
        print(f'Message is ({data})')
